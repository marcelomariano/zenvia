package com.zenvia.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.zenvia.utis.JsonUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "atm")
public class Atm implements IPojo<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5708345715831451601L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany(targetEntity = Cash.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Builder.Default
	@Wither
	private Set<Cash> caches = new HashSet<>();

	@Transient
	public int getTotalCash() {
		List<Integer> total = new ArrayList<Integer>();
		this.caches.stream().forEach(c -> {
			total.add(Math.multiplyExact(c.getQuantity(), c.getType().getValue()));
		});
		return total.stream().mapToInt(Integer::intValue).sum();
	}

	@Override
	public String toString() {
		return JsonUtil.toJsonPretty(this);
	}
}
