package com.zenvia.model;

import java.io.Serializable;

public interface IPojo<T> extends Serializable {
	T getId();
}