package com.zenvia.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.zenvia.enums.CashType;
import com.zenvia.utis.JsonUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "cash")
public class Cash implements IPojo<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4422591089354133276L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "quantity")
	@NotNull(message = "O campo quantity nao pode ser nulo")
	@Min(value = 5, message = "Deve haver pelo menos {value} test{value > 5 ? 's' : ''} no campo quantity")
	private int quantity;

	@NotNull(message = "O campo type nao pode ser nulo")
	@Enumerated(EnumType.STRING)
	private CashType type;

	@Override
	public String toString() {
		return JsonUtil.toJsonPretty(this);
	}
}
