package com.zenvia.controller.endpoint;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zenvia.constants.Messages;
import com.zenvia.interfaces.services.AtmCashService;
import com.zenvia.model.Cash;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/v1/cash", produces = MediaType.APPLICATION_JSON_VALUE)
public class AtmCashControllerV1 implements com.zenvia.controller.web.AtmCashControllerV1 {

	private AtmCashService atmCashService;

	@Override
	@PostMapping(value = "/cashin")
	public ResponseEntity<?> cashIn(@Valid @RequestBody List<Cash> cashIns) {
		log.info("Cashin " + cashIns + " running ... ");
		atmCashService.cashIn(cashIns);
		log.info("CashIns " + cashIns + " " + Messages.SUCESSFULLY_REGISTER_MSG);
		return new ResponseEntity(cashIns, HttpStatus.OK);
	}

	@Override
	@GetMapping(value = "/cashout")
	public Set<Cash> getCaches(@PathVariable int value) {
		return atmCashService.getCashOut(value);
	}

}
