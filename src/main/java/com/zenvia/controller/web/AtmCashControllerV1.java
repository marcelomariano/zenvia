package com.zenvia.controller.web;

import java.util.List;
import java.util.Set;

import org.springframework.http.ResponseEntity;

import com.zenvia.model.Cash;

public interface AtmCashControllerV1 {

	ResponseEntity<?> cashIn(List<Cash> cashIns);

	Set<Cash> getCaches(int value);

}
