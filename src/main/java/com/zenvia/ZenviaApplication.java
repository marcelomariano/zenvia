package com.zenvia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenviaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZenviaApplication.class, args);
	}

}
