package com.zenvia.enums;

import static com.zenvia.constants.Messages.FIFTY_MSG;
import static com.zenvia.constants.Messages.FIVE_MSG;
import static com.zenvia.constants.Messages.HUNDRED_MSG;
import static com.zenvia.constants.Messages.TEN_MSG;
import static com.zenvia.constants.Messages.TWENTY_MSG;

import com.zenvia.utis.JsonUtil;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;;

@Getter
@AllArgsConstructor
@RequiredArgsConstructor
public enum CashType {

	HUNDRED_CASH_TYPE(100, HUNDRED_MSG) {
		@Override
		public int value() {
			return this.getValue();
		}
		public String descricao() {
			return this.getDescription();
		}
	},
	FIFTY_CASH_TYPE(50, FIFTY_MSG) {
		@Override
		public int value() {
			return this.getValue();
		}
		@Override
		public String descricao() {
			return this.getDescription();
		}
	},
	TWENTY_CASH_TYPE(20, TWENTY_MSG) {
		@Override
		public int value() {
			return this.getValue();
		}
		@Override
		public String descricao() {
			return this.getDescription();
		}
	},
	TEN_CASH_TYPE(10, TEN_MSG) {
		@Override
		public int value() {
			return this.getValue();
		}
		@Override
		public String descricao() {
			return this.getDescription();
		}
	},
	FIVE_CASH_TYPE(5, FIVE_MSG) {
		@Override
		public int value() {
			return this.getValue();
		}
		@Override
		public String descricao() {
			return this.getDescription();
		}
	};
	private int value;
	private String description;
	public abstract int value();
	public abstract String descricao();

	@Override
	public String toString() {
		return JsonUtil.toJsonPretty(this);
	}
}