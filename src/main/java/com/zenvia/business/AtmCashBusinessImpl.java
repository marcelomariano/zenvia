package com.zenvia.business;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.zenvia.enums.CashType;
import com.zenvia.interfaces.business.AtmCashBusiness;
import com.zenvia.model.Atm;
import com.zenvia.model.Cash;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class AtmCashBusinessImpl implements AtmCashBusiness {

	public Set<Cash> getCashOut(Atm atm, int cashOutValue) {
		Set<Cash> cashout = new HashSet<Cash>();
		if (atm.getTotalCash() >= cashOutValue) {
			List<CashType> cashTypes = atm.getCaches().stream().map(Cash::getType)
					.sorted(Comparator.comparingInt(CashType::getValue).reversed()).collect(Collectors.toList());
			AtomicInteger atomicInt = new AtomicInteger(new Integer(cashOutValue));
			cashTypes.stream().forEach(c -> {
				int totalCedules = atomicInt.get() / c.getValue();
				atomicInt.set(atomicInt.get() % c.getValue());
				if (totalCedules >= 0) {
					cashout.add(Cash.builder().quantity(totalCedules).type(c).build());
				}
			});
		}
		return cashout;
	}

	public String showTotalCashoutCedules(Set<Cash> cashesOutCedules) {
		StringBuilder builder = new StringBuilder();
		builder.append("Total de c�dulas:");
		cashesOutCedules.stream().forEach(c -> {
			builder.append("[" + c.getQuantity() + "] cedulas de " + c.getType().getValue());
		});
		return builder.toString();
	}
}
