package com.zenvia.constants;

import com.zenvia.exceptions.BusinessException;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Messages {

	public static final String SUCESSFULLY_REGISTER_MSG = "CashIn efetuado com sucesso";

	public static final String HUNDRED_MSG 				= "Notas de 100";
	public static final String FIFTY_MSG 				= "Notas de 50";
	public static final String TWENTY_MSG 				= "Notas de 20";
	public static final String TEN_MSG 					= "Notas de 10";
	public static final String FIVE_MSG 				= "Notas de 5";

	@SneakyThrows
	public static void validationException(String msg) {
		log.warn(msg);
		throw new BusinessException(msg);
	}

}