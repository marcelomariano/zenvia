package com.zenvia.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.zenvia.business.AtmCashBusinessImpl;
import com.zenvia.interfaces.repositories.AtmCashRepository;
import com.zenvia.interfaces.services.AtmCashService;
import com.zenvia.model.Atm;
import com.zenvia.model.Cash;

import lombok.RequiredArgsConstructor;

@Service
@Component
@RequiredArgsConstructor
public class AtmCashServiceImpl implements AtmCashService {

	private final AtmCashRepository atmCashRepository;
	private final AtmCashBusinessImpl businessImpl;

	@Override
	public List<Atm> findAll() {
		return StreamSupport.stream(
				Spliterators.spliteratorUnknownSize(atmCashRepository.findAll().iterator(), Spliterator.ORDERED), false)
				.collect(Collectors.toList());
	}

	@Override
	public Atm saveAtm(Atm atm) {
		return atmCashRepository.save(atm);
	}

	@Override
	public void cashIn(List<Cash> cashIns) {
		Optional<Atm> atm = findAll().stream().findFirst();
		atm.ifPresent(a -> {
			cashIns.stream().forEach(cashIn -> {
				a.getCaches().stream().filter(c -> c.getType() == cashIn.getType()).forEach(c -> {
					c.setQuantity(c.getQuantity() + cashIn.getQuantity());
				});
			});
			saveAtm(a);
		});
	}

	@Override
	public Set<Cash> getCashOut(int value){
		Optional<Atm> optional = findAll().stream().findFirst();
		List<Cash> cashOut = businessImpl.getCashOut(optional.get(), value).stream().collect(Collectors.toList());
		optional.ifPresent(a -> {
			List<Cash> caches = a.getCaches().stream().collect(Collectors.toList());
			caches.stream().forEach(c -> {
				cashOut.stream().forEach(cOut -> {
					if (c.getType() == cOut.getType()) {
						c.setQuantity(Math.subtractExact(c.getQuantity(), cOut.getQuantity()));
					}
				});
			});
		});

		return cashOut.stream().collect(Collectors.toSet());
	}

}