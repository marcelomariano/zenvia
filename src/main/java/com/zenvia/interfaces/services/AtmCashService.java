package com.zenvia.interfaces.services;

import java.util.List;
import java.util.Set;

import com.zenvia.model.Atm;
import com.zenvia.model.Cash;

public interface AtmCashService {

	List<Atm> findAll();
	Atm saveAtm(Atm atm);
	void cashIn(List<Cash> cashIns);
	Set<Cash> getCashOut(int value);

}