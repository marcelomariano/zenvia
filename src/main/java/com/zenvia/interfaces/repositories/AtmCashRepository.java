package com.zenvia.interfaces.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.zenvia.model.Atm;

@Repository
public interface AtmCashRepository extends CrudRepository<Atm, Long> {

}