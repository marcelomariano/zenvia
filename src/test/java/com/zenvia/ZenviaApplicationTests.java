package com.zenvia;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.zenvia.enums.CashType;
import com.zenvia.interfaces.business.AtmCashBusiness;
import com.zenvia.model.Atm;
import com.zenvia.model.Cash;
import com.zenvia.services.AtmCashServiceImpl;

@ExtendWith(MockitoExtension.class)
class ZenviaApplicationTests {

	@InjectMocks private AtmCashServiceImpl atmService;
	@Mock private AtmCashBusiness atmBusiness;

	private Set<Cash> caches;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		caches = new HashSet<>();
		Cash c1 = Cash.builder().type(CashType.HUNDRED_CASH_TYPE).quantity(1).build();
		Cash c2 = Cash.builder().type(CashType.FIFTY_CASH_TYPE).quantity(1).build();
		Cash c3 = Cash.builder().type(CashType.TWENTY_CASH_TYPE).quantity(1).build();
		Cash c4 = Cash.builder().type(CashType.TEN_CASH_TYPE).quantity(1).build();
		Cash c5 = Cash.builder().type(CashType.FIVE_CASH_TYPE).quantity(1).build();
		caches.add(c1);
		caches.add(c2);
		caches.add(c3);
		caches.add(c4);
		caches.add(c5);

		Atm atm = Atm.builder().caches(caches).build();

	}

	@Test
	void shouldAddCashTest() {
//		atmService.cashIn(caches.stream().collect(Collectors.toList()));
		Assertions.assertNotNull(caches);
	}

}
